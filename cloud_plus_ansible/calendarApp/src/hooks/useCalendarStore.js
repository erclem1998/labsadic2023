import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import { calendarApi } from "../api";
import { convertEventsToDateEvents, getEnvVariables } from "../helpers";
import { onAddNewEvent, onLoadEvents, onSetActiveEvent } from "../store/calendar/calendarSlice";
const { VITE_API_URL } = getEnvVariables()

export const useCalendarStore = () => {

    const { events, activeEvent } = useSelector(state => state.calendar)
    const dispatch = useDispatch()

    const setActiveEvent = (calendarEvent) => {
        dispatch(onSetActiveEvent(calendarEvent))
    }

    const startSavingEvent = async (calendarEvent) => {
        try {
            // creando
            const { data } = await calendarApi.post(`${VITE_API_URL}:3001/api/events/create-event`, calendarEvent)
            const { start, end, ...rest } = calendarEvent;
            const newEvent = { ...rest, startDate: start, endingDate: end, idevent: data.newEvent.idevent };
            
            dispatch(onAddNewEvent({ ...newEvent}))
        } catch (error) {
            console.log(error)
            Swal.fire('Error al guardar', error.response.data.msg, 'error')
        }

    }

    const startLoadingEvents = async () => {
        try {
            const { data } = await calendarApi.get(`${VITE_API_URL}:3001/api/events`)
            const events = convertEventsToDateEvents(data.events);
            dispatch(onLoadEvents(events))
            // console.log({events})
        } catch (error) {
            console.log('Error cargando eventos')
            console.log(error)
        }
    }

    return {
        activeEvent,
        events,
        hasEventSelected: !!activeEvent,

        setActiveEvent,
        startLoadingEvents,
        startSavingEvent,
    }
}
