import { createSlice } from '@reduxjs/toolkit'
// import { addHours } from 'date-fns';

// const tempEvent = {
//     _id: new Date().getTime(),
//     title: 'Cumpleaños del jefe',
//     notes: 'Hay que comprar el paster',
//     start: new Date(),
//     end: addHours(new Date(), 2),
//     bgColor: '#fafafa',
//     user: {
//         _id: '123',
//         name: 'Erick'
//     }
// }

export const calendarSlice = createSlice({
    name: 'calendar',
    initialState: {
        isLoadingEvents: true,
        events: [
            // tempEvent
        ],
        activeEvent: null,
    },
    reducers: {
        onSetActiveEvent: (state, { payload }) => {
            state.activeEvent = payload
        },
        onAddNewEvent: (state, { payload }) => {
            state.events.push(payload);
            state.activeEvent = null
        },
        onLoadEvents: ( state, { payload = [] } ) => {
            state.isLoadingEvents = false;
            // state.events = payload;
            payload.forEach(event => {
                const exists = state.events.some( dbEvent => dbEvent.idevent === event.idevent )
                if (!exists) {
                    state.events.push(event)
                }
            });
        },
        onLogoutCalendar: ( state ) => {
            state.events = [];
            state.isLoadingEvents = true;
            state.activeEvent = null
        }
    }
});


// Action creators are generated for each case reducer function
export const {
    onAddNewEvent,
    onLoadEvents,
    onLogoutCalendar,
    onSetActiveEvent,
} = calendarSlice.actions;