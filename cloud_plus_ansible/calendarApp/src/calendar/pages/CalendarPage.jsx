import { useEffect, useState } from 'react';
import { Calendar } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css'

import { Navbar, CalendarEvent, CalendarModal, FabAddNew } from "../";
import { localizer, getMessagesES } from '../../helpers';
import { useCalendarStore } from '../../hooks';


export const CalendarPage = () => {

    const { events, startLoadingEvents } = useCalendarStore();

    const [lastView] = useState(localStorage.getItem('lastView') || 'month')

    const onViewChanged = (event) => {
        localStorage.setItem('lastView', event)
    }

    useEffect(() => {
        startLoadingEvents();
    }, [])


    return (
        <>
            <Navbar />
            <Calendar
                localizer={localizer}
                events={events}
                startAccessor="startDate"
                defaultView={lastView}
                endAccessor="endingDate"
                style={{ height: 'calc( 100vh - 80px )' }}
                culture='es'
                messages={getMessagesES()}
                // eventPropGetter={eventStyleGetter}
                components={{
                    event: CalendarEvent
                }}
                // onDoubleClickEvent={onDoubleClick}
                // onSelectEvent={onSelect}
                onView={onViewChanged}
            />
            <CalendarModal />
            <FabAddNew />
        </>
    )
}
