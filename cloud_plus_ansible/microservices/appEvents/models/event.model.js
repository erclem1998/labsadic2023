const { DataTypes } = require('sequelize');
const { db } = require('../database/connection')

const Evento = db.define('EVENTO', {
    idevent: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement: true
    },
    startDate: {
        type: DataTypes.DATE
    },
    endingDate: {
        type: DataTypes.DATE
    },
    title: {
        type: DataTypes.STRING
    },
    notes: {
        type: DataTypes.STRING
    },
    uid: {
        type: DataTypes.NUMBER
    },
}, {
    tableName: 'EVENTO'
});

module.exports = {
    Evento
}