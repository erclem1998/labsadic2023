const { Router } = require('express')
//IMPORTAR LOS CONTROLLERS
const { loginController, createUserController, revalidateToken } = require('../controller/auth.controller');
// EXPRESS-VALIDATOR
const { check } = require('express-validator');
// IMPORTAR VALIDADOR DE ERRORESS
const { validateFields } = require('../middlewares/validate-fields');
const { validateJWT } = require('../middlewares/validate-jwt');

const router = Router();

router.post('/login', [
    check('email', 'El email es obligatorio').not().isEmpty(),
    check('passwd', 'La contraseña es obligatoria').not().isEmpty(),
    validateFields
], loginController)

router.post('/create-user', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('passwd', 'La contraseña es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').not().isEmpty(),
    validateFields
],createUserController );

router.get('/renew', validateJWT, revalidateToken)

module.exports = router