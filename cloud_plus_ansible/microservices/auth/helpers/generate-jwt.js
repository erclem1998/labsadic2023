const jwt = require ('jsonwebtoken')

const generateJWT = ({name, email, uid}) => {

    const payload = { 
        uid, 
        name, 
        email
    }

    return new Promise((resolve, reject)=>{
        jwt.sign(payload, process.env.SECRET_JWT_SEED, {
            expiresIn: '2h'
        }, (err, token)=>{
            if(err){
                console.log(err)
                reject(err)
            } else{
                resolve(token)
            }
    
        })
    })

}

module.exports = {
    generateJWT
}
