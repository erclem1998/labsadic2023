const { Usuario } = require('../models/user.model')

const getUserLogin = async( email = '', passwd = '' ) => {
    try {
        const user = await Usuario.findOne({
            where: {
                email,
                passwd
            }
        })
        if (user) {
            const { dataValues } = user;
            const { passwd: pass, ...rest } = dataValues
            return  {
                ...rest
            }
        }
        return null

    } catch (error) {
        console.log(error)
    }
}

const createUser = async (passwd = '', name = '', email = '') => {
    try {
        const userFind = await Usuario.findOne({
            where: {
                email,
            }
        })
        if (userFind) {
            return {
                ok: false,
                msg: 'El usuario ya existe'
            }
        }
        const user = new Usuario({
            passwd,
            name,
            email
        })

        const newUser = await (await user.save()).reload();
        const { dataValues } = newUser;
        const { passwd: pass, ...rest } = dataValues

        return {
            ok: true,
            user: {
                ...rest
            }
        }

    } catch (error) {
        
    }
}

module.exports = {
    getUserLogin,
    createUser
}