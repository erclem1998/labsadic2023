const { response } = require('express');
const { getUserLogin, createUser } = require('../helpers/db-auth-actions');
const { generateJWT } = require('../helpers/generate-jwt');

const loginController = async( req, res = response ) => {

    const { passwd, email } = req.body;
    const user = await getUserLogin( email, passwd );

    if (user) {
        const token = await generateJWT({...user})
        return res.status(200).send({
            ok: true,
            msg: 'Login exitoso',
            user,
            token
            
        })
        
    }
    return res.status(400).send({
        ok: false,
        msg: 'Usuario o contraseña inválidos',
        
    })

}

const createUserController = async (req, res = response) => {

    const { passwd, name, email } = req.body

    const { ok, user } = await createUser(passwd, name, email)

    if (ok) {
        const token = await generateJWT({...user})
        return res.status(200).send({
            ok,
            msg: 'Usuario credo exitosamente',
            user,
            token
        });
    }
    return res.status(400).send({
        ok,
        msg: 'El usuario ya existe',
    });


}

const revalidateToken = async(req, res = response) => {
    const { uid, name, email } = req;
    const token = await generateJWT({uid, name, email})
    return res.json({
        ok: true,
        user: {
            uid, 
            name,
            email
        },
        token
    })
}

module.exports = {
    loginController,
    createUserController,
    revalidateToken
}