'use strict'
require('dotenv').config({path: __dirname + '/.env/.env'})
const express = require('express')
const cors = require('cors')
// Importación de elementos de base de datos
const { dbConnection } = require('./database/connection')
const { PORT } = process.env

//CREAR EL SERVIDOR
const app = express();

// CONFIGURACIÓN DE CORS
app.use(cors())

// PARSEAAR EL BODY
app.use(express.json());

// REALIZAR CONEXIÓN A LA BASE DE DATOS
dbConnection();

app.use('/api/auth', require('./routes/auth.route'));

app.listen( PORT, () => {
    console.log(`Servidor corriendo en puerto ${PORT}`)
})