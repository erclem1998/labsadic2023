const { getUserLogin } = require("../../helpers/db-auth-actions");
require('jasmine')

describe('Pruebas sobre acciones de auth', () => {
    const helpers = {
        getUserLogin
    }
    it('Deberia obtener el usuario', async() => {
        // Arrange
        const data = {
            email: 'erick-email@fake.com', 
            name: 'Erick Lemus'
        }
        spyOn(helpers, 'getUserLogin').and.returnValue(data)
        // Act
        const resultado = helpers.getUserLogin('erick-email@fake.com', '123456')
        // Assert
        expect(resultado).toBe(data)
        expect(helpers.getUserLogin).toHaveBeenCalled()
    });
});