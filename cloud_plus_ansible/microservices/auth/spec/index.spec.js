const request = require('request')
const index = require('../index')
require('jasmine')

describe('Pruebas de integración sobre Login', () => {
    const user = {
        passwd: 'USER12345',
        email: 'algo1@algo.com'
    }
    it('realizar un login correcto', (done) => {
        request.post(
            'http://localhost:3000/api/auth/login',
            { json: true, body: user },
            (error, response) => {
                if (error) {
                    error
                }
                expect(response.statusCode).toEqual(200)
                expect(response.body.ok).toBeTruthy()
                expect(response.body.msg).toBe('Login exitoso');
                expect(response.body.token.length).toBeGreaterThan(10);
                done();
            }
        )
    });
    it('realizar un login incorrecto', (done) => {
        request.post(
            'http://localhost:3000/api/auth/login',
            { json: true, body: {...user, passwd: '124233ssf3'} },
            (error, response) => {
                if (error) {
                    error
                }
                expect(response.statusCode).toEqual(400)
                expect(response.body.ok).toBeFalsy()
                expect(response.body.msg).toBe('Usuario o contraseña inválidos');
                done();
            }
        )
    });
});