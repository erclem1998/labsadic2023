const { Router } = require('express')

const {  loginController, createUserController } = require('../controller/auth.controller')

const router = Router();

router.post('/login', loginController)
router.post('/create-user', createUserController)

module.exports = router;;