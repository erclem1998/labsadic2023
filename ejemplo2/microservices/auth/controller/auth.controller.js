const { response } = require('express')

const loginController = async(req, res = response)=> {
    const { passwd = '', email = '' } = req.body;

    if (passwd === '' || email === '') {
        return res.status(400).send({
            ok: false,
            msg: 'Login erróneo',
        })   
    }
    return res.status(200).send({
        ok: true,
        msg: 'Login correcto'
    })
}

const createUserController = async(req, res = response)=> {
    const { passwd = '', email = '', name = '' } = req.body;

    if (passwd === '' || email === '' || name === '') {
        return res.status(400).send({
            ok: false,
            msg: 'Registro erróneo',
        })   
    }
    return res.status(200).send({
        ok: true,
        msg: 'Registro correcto'
    })
}

module.exports = {
    loginController,
    createUserController
}