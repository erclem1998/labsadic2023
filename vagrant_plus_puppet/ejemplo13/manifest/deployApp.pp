# Copiar archivo de docker-compose
file { '/home/vagrant/docker-compose.prod.yml':
  ensure  => 'file',
  content => template('/etc/puppetlabs/code/environments/production/manifests/docker-compose.prod.yml'),
}

# Despliega docker-compose
exec { 'Ejecuta despliegue de docker-compose':
  command => '/usr/bin/sudo /usr/bin/docker compose -f /home/vagrant/docker-compose.prod.yml up --build -d',
  require => File['/home/vagrant/docker-compose.prod.yml']
}
