packer {
    required_plugins {
        amazon = {
            version = ">= 1.0.0"
            source  = "github.com/hashicorp/amazon"
        }
    }
}

variable "aws_access_key" {
    type = string
    default = env("AWS_ACCESS_KEY")
}

variable "aws_secret_key" {
    type = string
    default = env("AWS_SECRET_KEY")
}

source "amazon-ebs" "ejemplo17-server" {
    ami_name      = "node-server-ami"
    source_ami    = "ami-0fc5d935ebf8bc3bc"
    instance_type = "t2.micro"
    region        = "us-east-1"
    ssh_username  = "ubuntu"
    access_key    = var.aws_access_key
    secret_key    = var.aws_secret_key
}

build {

    sources = [
        "source.amazon-ebs.ejemplo17-server"
    ]

    provisioner "file" {
        source      = "./server"
        destination = "/home/ubuntu/server"
    }

    provisioner "shell" {
        script = "./install.sh"
    }   

}