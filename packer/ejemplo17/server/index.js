'use strict'
const express = require('express')
const cors = require('cors')

const app = express();

app.use(cors());

app.use(express.json());

app.get('/', (req, res)=> {
    return res.status(200).send('<h1>Hola Laboratorio de SA!! Este es el ejemplo 17 utilizando Packer y un servidor de Node.js</h1>')
})

app.listen(3000, () => {
    console.log('Servidor corriendo en puerto 3000')
})