packer {
    required_plugins {
        amazon = {
            version = ">= 1.0.0"
            source  = "github.com/hashicorp/amazon"
        }
    }
}

variable "aws_access_key" {
    type = string
    default = env("AWS_ACCESS_KEY")
}

variable "aws_secret_key" {
    type = string
    default = env("AWS_SECRET_KEY")
}

source "amazon-ebs" "ejemplo18-docker" {
    ami_name      = "docker-app-ami"
    source_ami    = "ami-0fc5d935ebf8bc3bc"
    instance_type = "t2.micro"
    region        = "us-east-1"
    ssh_username  = "ubuntu"
    access_key    = var.aws_access_key
    secret_key    = var.aws_secret_key
}

build {

    sources = [
        "source.amazon-ebs.ejemplo18-docker"
    ]

    provisioner "file" {
        source      = "./docker-compose.prod.yml"
        destination = "/home/ubuntu/docker-compose.prod.yml"
    }

    provisioner "shell" {
        script = "./install.sh"
    }   

}