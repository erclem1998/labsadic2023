packer {
    required_plugins {
        amazon = {
            version = ">= 1.0.0"
            source = "github.com/hashicorp/amazon"
        }
    }
}

source "amazon-ebs" "ubuntu-ejemplo15" {
    ami_name      = "ejemplo15-nginx-ubuntu"
    source_ami    = "ami-0fc5d935ebf8bc3bc"
    instance_type = "t2.micro"
    region        = "us-east-1"
    ssh_username  = "ubuntu"
    access_key    = "AKIA3JIGD3TFFN34WODC"
    secret_key    = "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"
}

build {
    sources = [
        "source.amazon-ebs.ubuntu-ejemplo15"
    ]

    provisioner "shell" {
        script = "./install.sh"
    }
}